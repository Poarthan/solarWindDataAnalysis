import sys
import numpy as np
import matplotlib.pyplot as plt

from scipy import fftpack, signal
from tqdm import tqdm
import matplotlib as mpl

mpl.rcParams['agg.path.chunksize'] = 5000000000000


def main():

    #Loading Data
    #acef='ace-dat.txt'
    #acef='modified_ACE_data.txt'
    acef='prayACE.txt'
    acetf='ace-time.txt'

    #aced=np.loadtxt(fname=acef, usecols=range(3))
    aced=np.loadtxt(fname=acef, usecols=range(1))
    acet=np.loadtxt(fname=acetf)

    #a=np.array(aced[0:, 1])

    a=np.array(aced)
    at=np.array(acet)
    assert len(a)==len(at)
 
    #lisaf='lisa-dat.txt'
    #lisaf='modified_LISA_data.txt'
    lisaf='prayLISA.txt'
    lisatf='lisa-time.txt'

    lisad=np.loadtxt(fname=lisaf)
    lisat=np.loadtxt(fname=lisatf, usecols=range(2))

    l=np.array(lisad)
    lt=np.array(lisat[0:, 0])
    assert len(l)==len(lt)

    for i in range(at.size):
        if at[i]>lt[0]-1:
            at=at[i-1:]
            a=a[i-1:]
            break
    for i in range(at.size):
        if at[i] > lt[-1] + 1:
            at=at[:i]
            a=a[:i]
            break
    #ace=np.fft.fft(a)
    a=np.interp(lt, at, a)
    #lisa=np.fft.fft(xxx)
    #print(lisa.size, ace.size)
    plt.rcParams["figure.figsize"] = (16,11)
    mpl.rcParams['agg.path.chunksize'] = 500000000000000000
    plt.rcParams['agg.path.chunksize'] = 500000000000000000
    plt.rcParams.update({'font.size': 22})
    #figure, axis = plt.subplots(2,1)
    #axis[0].plot(array)
    fs=2*1/(16384/(3276*2))
    nn=lt.size//2
    print(nn)
    plt.title("Coherence of ACE and LPF")
    plt.xlabel("Frequency (Hz)")
    plt.ylabel("Coherence")
    plt.grid()
    #
    for i in range(15):
        nn=lt.size//2
        nn=nn//(2**i)
        nn=int(nn)
        print(nn, i, "WHAT IS THE PROBLEM")
        f, Cxy = signal.coherence(a, l, fs, nperseg=int(nn))
        print(f.size, Cxy.size)
        print(Cxy)
        #plt.figure(dpi=500)
        plt.loglog(f, Cxy)

        plt.title("Coherence of ACE and LPF Test Data")
        plt.xlabel("Frequency (Hz)")
        plt.ylabel("Coherence")
        plt.grid()

        ftypes=['png']
        saveplot(f'test-plots/co-tests/test-dat-co-2xx{i+1}', ftypes)
#        plt.show()

    #plt.xlabel('frequency [Hz]')
    nn=lt.size//32
    f, Cxy = signal.coherence(a, l, fs, nperseg=nn)
    print(f.size, Cxy.size)
    print(Cxy)
    #plt.figure(dpi=500)
    #plt.plot(f, Cxy)
    #plt.show()
    plt.loglog(f, Cxy)
    plt.axvline(x=1e-3)
    #plt.ylabel('Coherence')
    plt.title("Coherence of ACE and LPF Test Data")
    plt.xlabel("Frequency (Hz)")
    plt.ylabel("Coherence")
    plt.grid()

    #plt.show()
    '''
    Cxy, freqs=plt.cohere(xxx, a)
    plt.show()
    print(Cxy, freqs)
    plt.plot(Cxy, freqs)
    '''

    #saving plot
    #ftypes=['jpg']
    ftypes=['png']
    saveplot(f'test-plots/cohere-test', ftypes)

    plt.show()


def saveplot(title, filetypes):
    for ftype in filetypes:
        filename=f'{title}.{ftype}'
        print(f'saving file {filename}')
        plt.savefig(filename)

main()
