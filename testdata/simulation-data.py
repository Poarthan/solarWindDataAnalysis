#!/usr/bin/env python3

import sys
import numpy as np
import matplotlib.pyplot as plt
from scipy import fftpack
from datetime import datetime, timedelta
import time
import random
import math
from tqdm import tqdm


def main():

    lisaf='lisa-dat.txt'
    lisatf='lisa-time.txt'
    
    lisad=np.loadtxt(fname=lisaf)
    lisat=np.loadtxt(fname=lisatf, usecols=range(2))
    
    
    LISA_data=np.array(lisad)
    LISA_times=np.array(lisat[0:, 0])
    assert len(LISA_data)==len(LISA_times)

    #j=1e-6
    #l=1e-1
    #z=(l-j)//len(LISA_data)

    for i in tqdm(range(len(LISA_data))):
        for z in range(1,10**5,100):
            j=z*1e-6
            LISA_data[i] = LISA_data[i] + simulate(LISA_times[i],j,j)

    #np.savetxt('modified_LISA_data.txt', LISA_data)

    np.savetxt('prayLISA.txt', LISA_data)

    #Loading Data
    acef='ace-dat.txt'
    acetf='ace-time.txt'
    aced=np.loadtxt(fname=acef, usecols=range(3))
    acet=np.loadtxt(fname=acetf)
    
    ACE_data=np.array(aced[0:, 1])
    ACE_times=np.array(acet)
    assert len(ACE_data) == len(ACE_times)
    #j=1e-6
    #l=1e-1
    #z=(l-j)//len(ACE_data)
    #for i in tqdm(range(len(ACE_data))):
    #    #print("Ace", ACE_data[i])
    #    #print("simulation",simulate(ACE_times[i]))
    #    ACE_data[i] = 0 + simulate(ACE_times[i],z+j)


    for i in range(len(ACE_data)):
        for z in tqdm(range(1,10**5,100)):
            j=z*1e-6
    #    #print("Ace", ACE_data[i])
    #    #print("simulation",simulate(ACE_times[i]))
            ACE_data[i] = ACE_data[i]+ simulate(ACE_times[i],j,j)

    #np.savetxt('modified_ACE_data.txt', ACE_data)
    np.savetxt('prayACE.txt', ACE_data)


def simulate(times, freq=None, amplitude=None):
    if freq is None:
        freq= 1e-1
    if amplitude is None:
        amplitude=1e-6
    phase=0
    excitation = amplitude * np.sin(2 * np.pi * freq * times + phase)
    return excitation

if __name__ == '__main__':
    main()
