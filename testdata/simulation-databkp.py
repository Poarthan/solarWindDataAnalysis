#!/usr/bin/env python3

import sys
import numpy as np
import matplotlib.pyplot as plt
from scipy import fftpack
from datetime import datetime, timedelta
import time
import random
import math
from tqdm import tqdm


def main():

    #Loading Data
    acef='ace-dat.txt'
    acetf='ace-time.txt'
    aced=np.loadtxt(fname=acef, usecols=range(3))
    acet=np.loadtxt(fname=acetf)
    
    ACE_data=np.array(aced[0:, 1])
    ACE_times=np.array(acet)
    assert len(ACE_data) == len(ACE_times)
    #j=1e-6
    #l=1e-1
    #z=(l-j)//len(ACE_data)
    #for i in tqdm(range(len(ACE_data))):
    #    #print("Ace", ACE_data[i])
    #    #print("simulation",simulate(ACE_times[i]))
    #    ACE_data[i] = 0 + simulate(ACE_times[i],z+j)

    #for i in tqdm(range(len(ACE_data))):
    #    if i < len(ACE_data)//6:
    #        ACE_data[i] = 0 + simulate(ACE_times[i],1e-1)
    #    elif i < len(ACE_data)//3:
    #        ACE_data[i] = 0 + simulate(ACE_times[i],1e-2)
    #    elif i < len(ACE_data)//2:
    #        ACE_data[i] = 0 + simulate(ACE_times[i],1e-3)
    #    elif i < len(ACE_data)//1.5:
    #        ACE_data[i] = 0 + simulate(ACE_times[i],1e-4)
    #    elif i < len(ACE_data)//1.2:
    #        ACE_data[i] = 0 + simulate(ACE_times[i],1e-5)
    #    else:
    #        ACE_data[i] = 0 + simulate(ACE_times[i],1e-6)
    

    for i in tqdm(range(len(ACE_data))):
        #print("Ace", ACE_data[i])
        #print("simulation",simulate(ACE_times[i]))
        ACE_data[i] = 0 + simulate(ACE_times[i])
    #np.savetxt('modified_ACE_data.txt', ACE_data)
    np.savetxt('prayACE.txt', ACE_data)


    lisaf='lisa-dat.txt'
    lisatf='lisa-time.txt'
    
    lisad=np.loadtxt(fname=lisaf)
    lisat=np.loadtxt(fname=lisatf, usecols=range(2))
    
    
    LISA_data=np.array(lisad)
    LISA_times=np.array(lisat[0:, 0])
    assert len(LISA_data)==len(LISA_times)

    #j=1e-6
    #l=1e-1
    #z=(l-j)//len(LISA_data)
    #for i in tqdm(range(len(LISA_data))):
    #    LISA_data[i] = 0 + simulate(LISA_times[i],z+j)

    for i in tqdm(range(len(LISA_data))):
        LISA_data[i] = 0 + simulate(LISA_times[i])

    #for i in tqdm(range(len(LISA_data))):
    #    if i < len(LISA_data)//6: 
    #        LISA_data[i] = 0 + simulate(LISA_times[i], 1e-1)

    #    elif i < 2*len(LISA_data)//6: 
    #        LISA_data[i] = 0 + simulate(LISA_times[i], 1e-2)
    #    elif i < 3*len(LISA_data)//6:
    #        LISA_data[i] = 0 + simulate(LISA_times[i], 1e-3)
    #    elif i < 4*len(LISA_data)//6:
    #        LISA_data[i] = 0 + simulate(LISA_times[i], 1e-4)

    #    elif i < 5*len(LISA_data)//6:
    #        LISA_data[i] = 0 + simulate(LISA_times[i], 1e-5)
    #    else:
    #        LISA_data[i] = 0 + simulate(LISA_times[i], 1e-6)

    #np.savetxt('modified_LISA_data.txt', LISA_data)

    np.savetxt('prayLISA.txt', LISA_data)

def simulate(times, freq=None):
    if freq is None:
        freq= 1e-3
    amplitude = 1e-9
    phase=0
    excitation = amplitude * np.sin(2 * np.pi * freq * times + phase)
    return excitation

if __name__ == '__main__':
    main()
